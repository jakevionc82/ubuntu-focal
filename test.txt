fakeroot debootstrap --foreign --components=universe,main,restricted,multiverse --include=bc,bison,build-essential,ccache,curl,flex,g++-multilib,gcc-multilib,git,gnupg,gperf,imagemagick,lib32ncurses-dev,lib32readline-dev,lib32z1-dev,liblz4-tool,libncurses5,libncurses5-dev,libsdl1.2-dev,libssl-dev,libxml2,libxml2-utils,lzop,pngcrush,rsync,schedtool,squashfs-tools,xsltproc,zip,zlib1g-dev --verbose focal focal/ http://archive.ubuntu.com/ubuntu | tee -a debootstrap.txt


fakechroot fakeroot chroot focal/



/debootstrap/debootstrap --second-stage | tee -a /debootstrap/secondstage.txt



# mount --rbind /dev  focal/dev
# mount --rbind /proc focal/proc
# mount --rbind /sys  focal/sys
